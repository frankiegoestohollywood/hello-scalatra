package com.qa

import org.scalatra._
import java.net.URL
import scalate.ScalateSupport

class MyScalatraFilter extends ScalatraFilter with ScalateSupport {

//if changing any of the code in the requests, it needs to be valid
//HTML to compile

  //root context
  get("/") {
    <html>
      <body>
        <h1>Hello, world</h1>
      </body>
    </html>
  }

  get("/hello") {
    <html>
      <body>
        <h1>Hello world! Hah.</h1>
        Saying hello to Scalatra from Maven (test 2) hi there! <br/>
        I like pie. BUT NOT RABBIT PIE! <br/>
        I like boats! but only ones with sails <br />
        And I can't write HTML it turns out... <br />
        or tick boxes <br />

        <h2 style="color:red">HELLO SEATTLE!!!</h2>
        <p>Exciting message, your ship has come in</p>

        <h2>Bitbucket broke today. Sadness</h2>
        <p>Bitbucket uses [server:8080]/bitbucket-hook/, but github uses [server:8080]/github-webhook/</p>
        <p>Gitlab generates you a project link, use that!</p>
        <h2>Video Testing Hello!</h2>
      </body>
    </html>
  }

  //dinosaur context
  get("/dinosaur") {
	<html><body><h1> Triceratops are the best! </h1></body></html>
  }


  notFound {
    // If no route matches, then try to render a Scaml template
    val templateBase = requestPath match {
      case s if s.endsWith("/") => s + "index"
      case s => s
    }
    val templatePath = "/WEB-INF/scalate/templates/" + templateBase + ".scaml"
    servletContext.getResource(templatePath) match {
      case url: URL =>
        contentType = "text/html"
        templateEngine.layout(templatePath)
      case _ =>
        filterChain.doFilter(request, response)
    }
  }
}
